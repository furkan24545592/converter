
#include "converter.h"

int decimal = 0;
int binaryBase = 2;
int temp = 0;
int digits = 32;
std::vector<int> vec;

int getData(int userValue){
   decimal = userValue;

   for (int i = 1; i >= 0; i++) {         //for
     temp = decimal % binaryBase;

     if (temp >= 0) {
       vec.push_back(temp);
       decimal = decimal / binaryBase;
          if(decimal <= 0) {
             break;
         }
     }
     else
         break;
  }                                       //for

   if (vec.size() < digits) {                  //if
     int k = digits - vec.size();
     for (size_t i = 0; i < k; i++) {
       vec.push_back(0);
     }
  }                                      //if

   std::reverse(vec.begin(), vec.end());

   return 0;
}

void print(){
   for (std::vector<int>::const_iterator i = vec.begin(); i != vec.end(); ++i)
     std::cout << *i << ' ';
}
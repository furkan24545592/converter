/*
Bu başlık dosyası şu şekilde çağırılır:
#include "converter.h"

ve örnek oluşturulur:

int main()
{

  int temp;

  std::cout << "Decimal number: ";
  std::cin >> temp;

  getData(temp);
  print();


  return 0;
}
*/
#ifndef _CONVERTER_H
#define _CONVERTER_H
#endif

#include <iostream>
#include <vector>
#include <algorithm>


extern int decimal, binaryBase, temp, digits;
extern std::vector<int> vec;
int getData(int);
void print();
